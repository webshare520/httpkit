/**
 * @author mdc
 * @date 2017年7月5日 下午4:43:17
 */
package com0oky.httpkit.test;

/**
 * @author mdc
 * @date 2017年7月5日 下午4:43:17
 */
public class Accurate {

	private String displayLevel;
	private String domainName;
	private String status;

	/**
	 * @return 获取{@link #displayLevel}
	 */
	public String getDisplayLevel() {
		return displayLevel;
	}

	/**
	 * @param displayLevel 设置displayLevel
	 */
	public void setDisplayLevel(String displayLevel) {
		this.displayLevel = displayLevel;
	}

	/**
	 * @return 获取{@link #domainName}
	 */
	public String getDomainName() {
		return domainName;
	}

	/**
	 * @param domainName 设置domainName
	 */
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	/**
	 * @return 获取{@link #status}
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status 设置status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Accurate [displayLevel=" + displayLevel + ", domainName=" + domainName + ", status=" + status + "]";
	}
	
}
